package com.miniproject.demo.controller;

import java.time.Instant;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.ResetPassword;
import com.miniproject.demo.repository.ResetPasswordRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiResetPassword {

	@Autowired
	public ResetPasswordRepository resetPasswordRepository;
	
	@PostMapping("post/resetpassword")
	public ResponseEntity<Object> saveNewPassword(@RequestBody ResetPassword resetPass){
		
		resetPass.setCreatedBy("User");
		resetPass.setCreatedOn(Date.from(Instant.now()));
		resetPass.setIsDelete(false);
		
		ResetPassword resetData = this.resetPasswordRepository.save(resetPass);
		
		if(resetData.equals(resetPass)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
}
