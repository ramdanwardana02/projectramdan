package com.miniproject.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.LocationLevelModel;

public interface LocationLevelRepository extends JpaRepository<LocationLevelModel, Long> {

}
